# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Hôte: 127.0.0.1 (MySQL 5.6.22)
# Base de données: boutique
# Temps de génération: 2015-07-03 12:13:54 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Affichage de la table category
# ------------------------------------------------------------

DROP TABLE IF EXISTS `category`;

CREATE TABLE `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8_unicode_ci,
  `note` int(11) DEFAULT NULL,
  `cover` tinyint(1) DEFAULT NULL,
  `visible` tinyint(1) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;

INSERT INTO `category` (`id`, `title`, `description`, `note`, `cover`, `visible`, `created`)
VALUES
  (8,'Les Gaz nobles','On appelle gaz nobles, ou plus couramment gaz rares, les éléments chimiques du groupe 18',5,1,1,'2010-01-01 00:00:00'),
  (9,'Les Halogènes','Le terme halogène désigne les éléments chimiques de la 17e colonne du tableau périodique.',3,1,1,'2010-01-01 00:00:00'),
  (10,'Métal alcalin','Un métal alcalin est un élément chimique de la première colonne',4,1,1,'2010-01-01 00:00:00'),
  (11,'Métal alcalino-terreux','Un métal alcalino-terreux est un élément chimique de la deuxième colonne du tableau périodique des éléments.',2,1,1,'2010-01-01 00:00:00'),
  (12,'Métalloïde','Dès les début de la chimie et jusqu\'en 1965, le terme de métalloïde désigne un corps chimique.',1,0,1,'2010-01-01 00:00:00'),
  (13,'Ma categorie','Super categorie kfhqsdkhq sfiq siq',5,1,1,'2010-01-01 00:00:00');

/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;


# Affichage de la table product
# ------------------------------------------------------------

DROP TABLE IF EXISTS `product`;

CREATE TABLE `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `visible` tinyint(1) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_product_id` (`id`),
  UNIQUE KEY `uk_product_title` (`title`),
  KEY `IDX_D34A04AD12469DE2` (`category_id`),
  CONSTRAINT `FK_D34A04AD12469DE2` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;

INSERT INTO `product` (`id`, `category_id`, `title`, `description`, `visible`, `created`)
VALUES
  (1,8,'Xénon','Le xénon est un élément chimique, de symbole Xe et de numéro atomique 54. Le xénon est un gaz noble, inodore et incolore. Dans une lampe à décharge, il émet une lumière bleue.',1,'2010-01-01 00:00:00'),
  (2,8,'Krypton','Le krypton est un élément chimique, de symbole Kr et de numéro atomique 36. Il s\'agit d\'un gaz noble, inodore et incolore, découvert par Sir William Ramsay et Morris William Travers le 30 mai 18986 en réalisant une distillation de l\'air liquide. Étymologiquement, le nom de « krypton » dérive du grec ancien κρυπτός (kryptos) signifiant « caché ».',1,'2010-01-01 00:00:00'),
  (3,10,'Lithium','Le lithium est un métal alcalin et un élément chimique de symbole Li et de numéro atomique 3, situé dans le premier groupe du tableau périodique des éléments. Comme les autres métaux alcalins, le lithium métallique réagit facilement avec l\'air et avec l\'eau.',1,'2010-01-01 00:00:00'),
  (4,10,'kljslqsglqflkg','sfkgjlfjg fmg smfkgj pùr',0,'2010-01-01 00:00:00');

/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;


# Affichage de la table product_tag
# ------------------------------------------------------------

DROP TABLE IF EXISTS `product_tag`;

CREATE TABLE `product_tag` (
  `product_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  PRIMARY KEY (`product_id`,`tag_id`),
  KEY `IDX_E3A6E39C4584665A` (`product_id`),
  KEY `IDX_E3A6E39CBAD26311` (`tag_id`),
  CONSTRAINT `FK_E3A6E39C4584665A` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`),
  CONSTRAINT `FK_E3A6E39CBAD26311` FOREIGN KEY (`tag_id`) REFERENCES `tag` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `product_tag` WRITE;
/*!40000 ALTER TABLE `product_tag` DISABLE KEYS */;

INSERT INTO `product_tag` (`product_id`, `tag_id`)
VALUES
  (1,1),
  (1,2),
  (2,1),
  (2,2),
  (3,1),
  (3,2),
  (4,1),
  (4,2);

/*!40000 ALTER TABLE `product_tag` ENABLE KEYS */;
UNLOCK TABLES;


# Affichage de la table tag
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tag`;

CREATE TABLE `tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `word` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `tag` WRITE;
/*!40000 ALTER TABLE `tag` DISABLE KEYS */;

INSERT INTO `tag` (`id`, `word`)
VALUES
  (1,'atom'),
  (2,'atomes');

/*!40000 ALTER TABLE `tag` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
