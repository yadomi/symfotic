<?php

namespace Store\FrontendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Store\FrontendBundle\Entity\Category;
use Store\FrontendBundle\Form\CategoryType;


class CategoryController extends Controller
{
    public function indexAction() {

      $em = $this->getDoctrine()->getManager();
      $categories = $em->getRepository('StoreFrontendBundle:Category')->findAll();

      return $this->render('StoreFrontendBundle:Category:index.html.twig',
           ['categories' => $categories]
      );

    }


    public function newAction(Request $request) {
      
      $category = new Category();

      $form = $this->createForm(new CategoryType(), $category);
      $form->handleRequest($request);
      if($form->isValid()){
        $this->get('session')->getFlashBag()->add('notice', 'Votre categorie a bien été edité');
        $em = $this->getDoctrine()->getManager();
        $em->persist($category);
        $em->flush();
        return $this->redirectToRoute('store_frontend_category');
      }

      return $this->render('StoreFrontendBundle:Category:edit.html.twig', [
        'form' => $form->createView()
      ]);


    }


    public function editAction(Request $request, $id) {

      $em = $this->getDoctrine()->getManager();
      $category = $em->getRepository('StoreFrontendBundle:Category')
        ->findOneById($id);

      $form = $this->createForm(new CategoryType(), $category);
      $form->handleRequest($request);
      if($form->isValid()){
        $this->get('session')->getFlashBag()->add('notice', 'Votre categorie a bien été edité');
        $em->persist($category);
        $em->flush();
        return $this->redirectToRoute('store_frontend_category');
      }

      return $this->render('StoreFrontendBundle:Category:edit.html.twig', [
        'form' => $form->createView()
      ]);

    }

    public function removeAction(Request $request, $id) {

      $em = $this->getDoctrine()->getManager();
      $category = $em->getRepository('StoreFrontendBundle:Category')
        ->findOneById($id);

      $em->remove($category);
      $em->flush();

      $this->get('session')->getFlashBag()->add('notice', 'Votre categorie a bien été supprimée');
      return $this->redirectToRoute('store_frontend_category');

    }

}
