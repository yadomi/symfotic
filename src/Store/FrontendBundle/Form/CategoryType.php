<?php

namespace Store\FrontendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class CategoryType
 * @package Store\FrontendBundle\Form
 */
class CategoryType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', null, array(
                'label' => 'Nom de la categorie',
                'required' => true,
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => 'Nom de la categorie',
                    'pattern' => '[\S\s]{10,}'
                )
            ))
            ->add('description', null, array(
                'label' => 'Description de categorie',
                'required' => true,
                'attr' => array(
                    'class' => 'form-control',
                    'cols' => 10,
                    'rows' => 5,
                    'placeholder' => 'Description du categorie',
                )
            ))
            ->add('visible', null, array(
                'data' => true,
                'label' => 'Est-il visible?'
            ))
            ->add('note', 'number', array(
                'label' => 'Note',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))

            ->add('cover', null, array(
                'label' => 'Afficher sur la homepage ?'
            ))
            ->add('created', 'date')
            ->add('Enregistrer','submit', array(
                'attr' => array(
                    'class' => 'btn btn-primary'
                )
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Store\FrontendBundle\Entity\Category'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'store_frontendbundle_category';
    }
}
