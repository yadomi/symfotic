<?php
namespace Store\FrontendBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class CategoryRepository
 * @package Store\FrontendBundle\Repository
 */
class CategoryRepository extends EntityRepository
{


    public function getCategoryHomepage()
    {


    // @Symfomany pourquoi ne pas passer par Doctrine ?
    return $this->getEntityManager()
        ->createQuery(
        'SELECT  c.title, c.description
          FROM StoreFrontendBundle:Category c
          WHERE c.created < CURRENT_TIMESTAMP() AND
          c.visible = 1 AND 
          c.cover = 1
          ORDER BY c.note ASC'
        )
    ->setMaxResults(10)
    ->getResult();
    }
}



